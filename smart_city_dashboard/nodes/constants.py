class Vertical_Types:
    air_quality = 'AQ'
    water_flow = 'WF'
    water_distribution = 'WD'


class Constants:
    FIELDS = 'fields'
    NAME = 'name'
    LOCATION_NAME = 'location_name'
    COORDS = 'coordinates'
    DATA = 'data'