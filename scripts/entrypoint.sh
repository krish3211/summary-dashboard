#!/bin/bash
DIRNAME="smart_city_dashboard"
readonly DIRNAME
set -e

echo "---Collecting static files---"
python ${DIRNAME}/manage.py collectstatic --no-input

echo "---Checking for postgress db on ${POSTGRES_HOST:=localhost}:${POSTGRES_PORT:=5432}---"
sh scripts/wait-for.sh ${POSTGRES_HOST:=localhost}:${POSTGRES_PORT:=5432} -- echo "Postgres runnig on ${POSTGRES_HOST:=localhost}:${POSTGRES_PORT:=5432}"

echo "---Applying database migrations---"
python ${DIRNAME}/manage.py migrate --no-input

echo "---Starting server---"
echo "---init gurnicon---"
gunicorn --chdir ./${DIRNAME}